<?php
// TYPO3 Security Check
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// Let's configuration of this extension from "Extension Manager"
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$_EXTKEY] = unserialize($_EXTCONF);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rv_custom/Configuration/PageTSconfig/setup.typoscript">');

// Let's add default PageTS for "Form"
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['default'] = 'EXT:rv_custom/Configuration/RTE/Default.yaml';

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'RV.RvCustom',
            'Email',
            [
                'Emails' => 'new, list, create'
            ],
            // non-cacheable actions
            [
                'Emails' => 'create'
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        email {
                            iconIdentifier = rv_custom-plugin-email
                            title = LLL:EXT:rv_custom/Resources/Private/Language/locallang_db.xlf:tx_rv_custom_email.name
                            description = LLL:EXT:rv_custom/Resources/Private/Language/locallang_db.xlf:tx_rv_custom_email.description
                            tt_content_defValues {
                                CType = list
                                list_type = rvcustom_email
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'rv_custom-plugin-email',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:rv_custom/Resources/Public/Icons/user_plugin_email.svg']
			);
		
    }
);