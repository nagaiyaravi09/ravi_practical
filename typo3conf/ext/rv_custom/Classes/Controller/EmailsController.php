<?php
namespace RV\RvCustom\Controller;


/***
 *
 * This file is part of the "[RAVI] Simple Custom Ext" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 
 *
 ***/
/**
 * EmailsController
 */
class EmailsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * emailsRepository
     * 
     * @var \RV\RvCustom\Domain\Repository\EmailsRepository
     */
    protected $emailsRepository = null;

    /**
     * @param \RV\RvCustom\Domain\Repository\EmailsRepository $emailsRepository
     */
    public function injectEmailsRepository(\RV\RvCustom\Domain\Repository\EmailsRepository $emailsRepository)
    {
        $this->emailsRepository = $emailsRepository;
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $emails = $this->emailsRepository->findAll();
        $this->view->assign('emails', $emails);
    }

    /**
     * action new
     * 
     * @return void
     */
    public function newAction()
    {
    }

    /**
     * action create
     * 
     * @param \RV\RvCustom\Domain\Model\Emails $newEmails
     * @return void
     */
    public function createAction(\RV\RvCustom\Domain\Model\Emails $newEmails)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->emailsRepository->add($newEmails);
        $this->redirect('list');
    }
}
