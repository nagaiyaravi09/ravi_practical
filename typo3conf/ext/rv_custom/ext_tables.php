<?php
// TYPO3 Security Check
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}
// Extension key
$_EXTKEY = 'rv_custom';
// Add default include static TypoScript (for root page)
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', '[RV] template');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'RV.RvCustom',
    'Email',
    'Email Subscribers'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('rv_custom', 'Configuration/TypoScript', '[RAVI] Simple Custom Ext');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_rvcustom_domain_model_emails', 'EXT:rv_custom/Resources/Private/Language/locallang_csh_tx_rvcustom_domain_model_emails.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_rvcustom_domain_model_emails');