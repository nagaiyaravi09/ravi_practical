<?php
// Provide detailed information and depenencies of EXT:rv_custom
$EM_CONF['rv_custom'] = [
    'title' => '[RAVI] Simple Custom Ext',
    'description' => 'This Extension is use for Temaplating',
    'category' => 'templates',
    'author' => 'Ravi',
    'author_email' => 'nagaiyaravi09@gmail.com',
    'author_company' => 'RAVI',
    'state' => 'stable',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.0.0-10.9.99',
            'ns_basetheme' => '1.0.0-10.9.99',
            'gridelements' => '8.0.0-9.9.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
