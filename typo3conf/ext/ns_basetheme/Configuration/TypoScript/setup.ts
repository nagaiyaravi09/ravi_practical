# Includes Basic Configuration of website
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:ns_basetheme/Configuration/TypoScript/Configuration" extensions="ts">

# Includes Page Configuration
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:ns_basetheme/Configuration/TypoScript/Page" extensions="ts">