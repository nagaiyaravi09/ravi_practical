# Let's define some constants for global configuration
ns_basetheme {
    website {
        settings {
            }
        }

        # Define paths for templates, layout and partial
        paths {
            templateRootPath = typo3conf/ext/ns_basetheme/Resources/Private/Templates/
            layoutRootPath = typo3conf/ext/ns_basetheme/Resources/Private/Layouts/
            partialRootPath = typo3conf/ext/ns_basetheme/Resources/Private/Partials/
        }
    }
}