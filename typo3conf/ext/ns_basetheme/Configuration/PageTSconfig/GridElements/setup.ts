// Grids configuration added in GLOBAL pageTS
tx_gridelements.setup {
    # Identifier
    nsBase2Col {
        title = 2 Column Grid
        description = Standard two column grid container
        icon = EXT:ns_basetheme/Resources/Public/Icons/gridelements.svg
        flexformDS = FILE:EXT:ns_basetheme/Configuration/FlexForms/Gridelements/nsBase2Col.xml
        config {
            colCount = 2
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Column 1
                            colPos = 1
                        }
                        2 {
                            name = Column 2
                            colPos = 2
                        }
                    }
                }
            }
        }
    }
}
