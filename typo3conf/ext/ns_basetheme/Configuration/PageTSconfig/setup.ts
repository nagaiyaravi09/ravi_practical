// Include the BackendLayouts
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:ns_basetheme/Configuration/PageTSconfig/BackendLayouts" extensions="ts">

// Include the BackendLayouts
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:ns_basetheme/Configuration/PageTSconfig/TceForm" extensions="ts">

// Include the BackendLayouts
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:ns_basetheme/Configuration/PageTSconfig/GridElements" extensions="ts">